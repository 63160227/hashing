import java.util.*;

public class hash {

	    private int key;
	    private String value;
	    private String[] lookupTable = new String[100]; // set size = 100

	    public hash(int key, String value) {
	        this.key = key;
	        this.value = value;
	         lookupTable[hash(key)]=value;
	    }

	    public void put(int key,String value){   //configuration into lookupTable
	       lookupTable[hash(key)]=value;
	    }
	    
	    public int hash(int key){
	        return key%lookupTable.length;
	    }
	    
	    public String get(int key){
	        return (String) lookupTable[hash(key)];
	    }
	    public String remove(int index){
	        String temp =  lookupTable[hash(index)];
	        lookupTable[hash(index)]=null;
	        return temp;
	    }
	}

